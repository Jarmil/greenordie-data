FROM python:3.11-slim

RUN mkdir /app
WORKDIR /app
COPY ./*.py .
COPY ./requirements.txt .
COPY ./run-docker .
COPY ./web ./web
COPY ./main ./main
COPY ./api1 ./api1
COPY ./data ./data
COPY ./fixtures ./fixtures

RUN pip install -r requirements.txt

RUN bash -c 'adduser --disabled-login --quiet --gecos app app &&  \
             chmod -R o+r /app/ && \
             mkdir /app/staticfiles && \
             chown -R app:app /app/staticfiles && \
             chmod o+x /app/run-docker && \
             touch /app/god.log && \
             chmod o+w /app/god.log'

USER app

ENV DJANGO_SETTINGS_MODULE "main.settings.production"

# fake values for required env variables used to run collectstatic during build
RUN DJANGO_SECRET_KEY=x DATABASE_URL=postgres://x/x DJANGO_ALLOWED_HOSTS=x \
    python manage.py collectstatic

CMD ["bash", "run-docker"]
