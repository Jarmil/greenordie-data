"""
Django's settings used in production environment
"""

from .base import *

DEBUG = False

# all zeros are necessary to run in container
ALLOWED_HOSTS = ["0.0.0.0", "127.0.0.1"]
if EXTRA_ALLOWED_HOST:
    ALLOWED_HOSTS.append(EXTRA_ALLOWED_HOST)

# Necessary for Django > 4.  (otherwise forms even with csrf_token fail)
CSRF_TRUSTED_ORIGINS = ['https://greenordie.org', 'https://www.greenordie.org']
