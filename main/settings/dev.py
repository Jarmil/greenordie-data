"""
Django settings for main project, DEV environment.
Docs: https://docs.djangoproject.com/en/4.1/topics/settings/
"""
import os
from .base import *
from pathlib import Path

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/4.1/howto/deployment/checklist/

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# localhost and 127.0.0.1 are for testing purposes
ALLOWED_HOSTS = ["localhost", "127.0.0.1"]
