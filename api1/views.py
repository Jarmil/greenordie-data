import logging
from django.http import JsonResponse
import urllib

from web import models


def queries(queryset):
    """Syntactic sugar"""
    return [x.query for x in queryset]


def cors_json_response(data, safe=True):
    response = JsonResponse(data, safe=safe)
    # IMP consider range of CORS allowed addresses
    response.headers["Access-Control-Allow-Origin"] = "*"
    return response


def random_entity(request):
    """returns random entity"""
    e = models.Entity.objects.all().order_by("?")[0]
    logging.debug(f"Random query, returned '{e.name}'")
    return cors_json_response({"results": [e.as_endpoint]})


def last_queries(request):
    """Return last searched phrases. Limit hardcoded below"""
    return cors_json_response(queries(models.Query.objects.all().order_by("-t")[:5]), safe=False)


def query(request, search):
    """endpoint for search"""
    search = urllib.parse.unquote(search).replace("+", " ")  # Unescape URL space coded old way
    logging.debug(f"Searching for '{search}'")
    return cors_json_response({
        "q": search,
        "results": [x.as_endpoint for x in models.search(search)]
    })
