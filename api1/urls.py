from django.urls import path

from . import views

urlpatterns = [
    path("q/<str:search>", views.query),
    path("random", views.random_entity),
    path("last", views.last_queries),
]


