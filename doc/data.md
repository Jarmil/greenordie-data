# Data notes and sources

Here are listed some interesting sources of data, along with notes about what is missing in the database.
Consider creating a script for bulk data import.

## What data we are missing

More clothes (skirts etc.). Based on its weight.  
Car emissions: add 2.23 g/km (from tire footprint and mileage). For SUV it is about 4.06 g/km


## Interesting data sources:

### To import:

Food (for Germany): https://www.ifeu.de/fileadmin/uploads/Reinhardt-Gaertner-Wagner-2020-Oekologische-Fu%c3%9fabdruecke-von-Lebensmitteln-und-Gerichten-in-Deutschland-ifeu-2020.pdf  
(Imported data for (major) vegetables and fruit, up to page 10.)


### Already imported:

Global numbers per states (and other entities), structured, JSON: https://ourworldindata.org/co2-emissions  
All states + some regions, imported latest data (mostly from 2021) 

Some clothing estimates: https://www.frontiersin.org/articles/10.3389/fenvs.2022.973102/full,
based on this study: https://search.issuelab.org/resources/29099/29099.pdf  (t-shirts, co2 per kg of fabric, rubber, leather, shoes)


