# GreenOrDie.org project

Django based.  This codument describes programming part of the project.  
Data issues and notes [are here](doc/data.md)  

## Structure


```/doc```      documentation    
```/web```      base app providing web interface for end user  
```/api1```     app providing api  
```/data```     'admin' kind of app for sorting and adding data  
```/tools```    hosts helper tools, now there is just one tool:    
    ```/tools/wikimport.py```   imports SPARQL queries. To get help, run it with ```-h``` argument 
```/fixtures``` some base data. Imported via management command importdata  


## Installation

    virtualenv -p python3.11 .venv
    source .venv/bin/activate
    make pipi

## Notes for developers

Common dev actions are defined in Makefile. Type ```make```  to see actual list. 

Application is distributed as a docker image. 
We use docker's opensource brother ```podman``` for build.   

To compile image, type ```make image```, it takes Dockerfile and builds image named "greenordie"


## Todos

In this early phase of development, todos are held in code. Type ```make todo``` to see actual todo list.
To addd a todo, simply add line as follows. General todos are here, code-specific in code itself.

- IMP add personday icons to results  
- IMP User Tags (e.g. "food" - select from pre-imported tags)  
- IMP Comment result
- IMP Add "random search"
- IMP /robots.txt 
- IMP /sitemap.xml 
- TODO in 'missing data' area (bottom right on homepage) do not list already existing data (like "Ghana").
- IMP models.EntityVote: Use them either in admin area, or perform automatic Entity creation from it.
- TODO search in "Aliases" and Entity names should both be case-insensitive (I guess aliases are not)
- IMP add "none of above" button to 'add an alias' feature
- IMP add autodate to models.Footprint, list it in results
- IMP Add citations, see [inspiration](doc/quotes-inspiration.png)
- IMP After all votes, redirect back to results (not to thankyou page, use messages instead) 
- IMP Query PRG-IST (returns CO2 emissions for one person between airports)
