""" Fixtures used in tests"""

import pytest

import web.models as model


@pytest.fixture
def entities():
    return model.Entity.objects.all()


