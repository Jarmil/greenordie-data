import pytest
from django.urls import reverse

pytestmark = pytest.mark.django_db


def test_index(client):
    response = client.get("/")
    assert response.status_code == 200
    assert "carbon" in response.content.decode()


def test_help(auth_client):
    assert auth_client.get(reverse("help")).status_code == 200


def test_repos_data(auth_client):
    assert auth_client.get(reverse("repos-data")).status_code == 200


def test_thankyou(auth_client):
    assert auth_client.get(reverse("thankyou")).status_code == 200


def test_q(auth_client):
    assert auth_client.get("/q/car").status_code == 200


def test_tag(auth_client):
    assert auth_client.get("/tag/Food").status_code == 200
    assert auth_client.get("/tag/NonExistingTag").status_code == 302
