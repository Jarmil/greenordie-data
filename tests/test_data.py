""" Here comes data quality (and integrity) tests.
    Tests should give readable results for non-pythoners,
    so preferred way is to raise AssertionError with descriptive text,
    instead of using 'plain' asserts.
"""

import pytest

from tests.fixtures import entities

import web.models as model

pytestmark = pytest.mark.django_db


def test_sources(entities):
    """ Every data should cite a source """
    for e in entities:
        if not e.data["footprint"].get("source", None):
            raise AssertionError(f"Missing source in entity '{e.name}'")


def test_units(entities):
    """ Make sure there is no 'weird' unit """
    allowed_units = ["km", "unit", "personday", "personkm", "kg", "portion", "pair", "m2"]
    for e in entities:
        unit = e.data["footprint"].get("per")
        if unit not in allowed_units:
            raise AssertionError(f"Unknown unit '{unit}' in '{e.name}', must be one of {', '.join(allowed_units)}")


def test_aliases(entities):
    """ no duplicity allowed in aliases + names"""
    used_aliases = []
    for e in entities:
        for x in e.names:
            if x in used_aliases:
                raise AssertionError(f"Name or alias '{x}' for entity '{e.name}' is already used elsewhere")
        used_aliases.extend(e.aliases)


def test_amount(entities):
    """ Every footprint data should have either 'amount' or 'min' and 'max' """
    for e in entities:
        fp = e.data["footprint"]
        if not (fp.get("amount") or all([fp.get("min"), fp.get("max")])):
            raise AssertionError(f"'{e.name}' must specify footprint data: either 'amount' or 'min and 'max' ")
