import pytest
from tests.fixtures import entities

pytestmark = pytest.mark.django_db


def test_names(client, entities):
    for e in entities:
        response = client.get(f"/api/v1/q/{e.name}")
        for entry in response.json["results"]:
            assert entry["name"]
