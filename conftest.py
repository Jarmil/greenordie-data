""" Pytest configuration, mostly defining own fixtures.
    Also loads test data into database via django_db_setup fixture
"""

import pytest
from django.core.management import call_command


@pytest.fixture()
def auth_client(client, django_user_model):
    """ Fixture: authenticated user """
    user = django_user_model.objects.create_user(username="foo", password="bar")
    client.force_login(user)
    return client


@pytest.fixture(scope='session')
def django_db_setup(django_db_setup, django_db_blocker):
    """ Fixture: initialized database """
    with django_db_blocker.unblock():
        call_command('loaddata', 'tests/fixtures/Tag.yaml')
