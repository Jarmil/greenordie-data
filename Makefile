help:
	@echo "Available actions:"
	@echo ""
	@echo " r           run server"
	@echo " rp          run web server in docker (podman), attached to terminal"
	@echo " srvstop     stop web server running in docker (podman)"
	@echo " test        run tests"
	@echo " testt       run tests beginning with testt_"
	@echo " cov         Show report for test code coverage"
	@echo ""
	@echo "Compilation:"
	@echo " clean       clean build files (perform before build)"
	@echo " image       build docker image (via podman)"
	@echo " imagefile   build docker image file for deployment"
	@echo ""
	@echo "Database:"
	@echo " migrations  Generate migrations"
	@echo " migrate     Run migrations"
	@echo ""
	@echo "DEV things"
	@echo " todo        List to do tasks"
	@echo " imp         list of improvements"
	@echo " pipi        install python modules from requirements.txt"
	@echo " black       reformat python code using black"


rp:
	podman run --rm --name=godorg \
	    -u root \
	    -v ./db.sqlite3:/app/db.sqlite3:Z \
	    -v ./god.log:/app/greenordie.log:Z \
	    -e DJANGO_SETTINGS_MODULE=main.settings.production \
	    -e DJANGO_SECRET_KEY \
	    -e EXTRA_ALLOWED_HOST \
	    -p 8000:8000 \
	    localhost/greenordie:latest

srvstop:
	podman stop $(podman ps | grep godorg | cut -b1-12)

test:
	pytest --ds=main.settings.dev

testt:
	pytest -k "testt_" --ds=main.settings.dev

cov:
	pytest --ds=main.settings.dev --cov=. --cov-report=term-missing --cov-config=pytest_coverage_config

pipi:
	pip install -r requirements.txt

black:
	black *.py --line-length 999

clean:
	rm greenordie_image.tar &2> /dev/null

image:
	podman build --tag=greenordie .

imagefile: clean image
	podman save greenordie > greenordie_image.tar

r:
	python manage.py runserver

migrations:
	python manage.py makemigrations

migrate:
	python manage.py migrate

todo:
	@rgrep -EI "TODO" | \
	    grep -v "\.venv" | \
	    grep -v "Makefile" | \
	    grep -v "\.git" | \
	    grep -v "raw/" | \
	    grep -v "static/bootstrap" | \
	    grep -v "web/static/fa"

imp:
	@rgrep -EI "^.{0,40}IMP\s" | grep -v "\.venv" | grep -v "\.git"

