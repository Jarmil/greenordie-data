""" Helper function(s) for project """


def client_ip_address(request):
    """ As name says. Client's IP.
        It does not handle the situation after proxy (FORWARDED_FOR), but we do not need it now
    """
    return request.META.get("REMOTE_ADDR", None)