from django.db import models
from django.utils import timezone
from strsimpy.levenshtein import Levenshtein
from django.contrib.auth.models import User


class _SlugQueryMixin:
    """ Adds <slug> property to models having a <query> field """
    @property
    def slug(self):
        return self.query.replace(" ", "+")


def search(query):
    """Searches database for <query>, returns list of found Entities"""
    found_aliases = [e.entity for e in Alias.objects.filter(name__iexact=query)]
    found_direct = [e for e in Entity.objects.filter(name__iexact=query)]

    if found_direct + found_aliases:
        results = found_direct + found_aliases
    else:
        # No exact match. Find most similar strings (only in names)
        results = []

    # Save to stats, and create ClassifiedQuery, if it does not exist yet
    # All new classified queries are supposed to be legal
    was_answered = True if results else False
    Query(query=query, answered=was_answered).save()
    if not was_answered:
        ClassifiedQuery.objects.get_or_create(query=query)

    return results


class _Vote(models.Model):
    """ Common ancestor for all votes """
    ip = models.GenericIPAddressField(null=True)  # IP address of end user
    t = models.DateTimeField(default=timezone.now)

    class Meta:
        abstract = True


class Entity(models.Model):
    """ One 'entity', for which we list footprint(s)
        Entity can be pretty much anything, real entity or group of entities with the same or similar footprint, e.g.:
        "World", "USA", "Beer", "Car", "Tomato"
    """
    name = models.TextField(unique=True)
    description = models.TextField(null=True)   # free text, e.g. "Big personal car"
    tags = models.ManyToManyField("Tag", related_name="entities")

    class Meta:
        ordering = ["name"]

    @property
    def as_endpoint(self):
        """Returns this object as it should be seen via API endpoint"""

        ret = {
            "id": self.id,
            "name": self.name,
            "desc": self.description,
        }

        # IMP there can be more than one footprint. Work with that
        try:
            ret["footprint"] = self.footprints.all()[0].as_endpoint
        except IndexError:
            ret["footprint"] = None

        # Restructure attributes: we want is a {name: values} hashtable. not nice, but works
        attributes = {}
        for x in self.attributes.all():
            a = x.as_endpoint
            name = a.pop("name")
            attributes[name] = a

        if attributes:
            ret["attributes"] = attributes

        return ret

    def distance_from(self, query):
        """String similarity: Returns Levenshtein distance of Entity name from query"""
        return Levenshtein().distance(self.name.lower(), query.lower())

    def __str__(self):
        return self.name


class Alias(models.Model):
    """ One alias name for Entity"""
    name = models.TextField(unique=True)
    entity = models.ForeignKey(Entity, related_name="aliases", on_delete=models.CASCADE)
    date_added = models.DateTimeField(auto_now_add=True, null=True)
    comment = models.TextField(null=True)


class AliasVote(_Vote):
    """ User content: proposed alias name for Entity """
    name = models.TextField(null=False)
    entity = models.ForeignKey(Entity, related_name="possible_aliases", on_delete=models.CASCADE)


class EntityVote(_Vote):
    """ User content: proposed that some name should or should not be an entity """
    name = models.TextField(null=False)
    is_entity = models.BooleanField(null=False)  # What user thinks


class TagVote(_Vote):
    """ User content: proposed tag for Entity """
    tag = models.ForeignKey("Tag", related_name="votes", on_delete=models.CASCADE)
    entity = models.ForeignKey(Entity, related_name="votes", on_delete=models.CASCADE)


class Footprint(models.Model):
    """ Describes one CO2 footprint """
    ALLOWED_UNITS = (
        ("km", "km"),
        ("unit", "unit"),
        ("personday", "person / day"),
        ("personkm", "person/km"),
        ("kg", "kg"),
        ("portion", "portion"),
        ("pair", "pair"),
        ("m2", "m2")
    )

    min = models.FloatField()  # if we know exact amount, min==max
    max = models.FloatField()
    per = models.TextField(choices=ALLOWED_UNITS)  # km, person, ...
    source = models.URLField(null=True)  # URL of the source
    note = models.TextField(null=True)  # mostly describes methodology
    entity = models.ForeignKey(Entity, related_name="footprints", on_delete=models.CASCADE)
    reviewed = models.BooleanField(null=False, default=False)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="footprints")

    @property
    def votes_score(self):
        """ Returns percentage value of <votes_positive>/<votes_total>, as an integer number in range 0..100
            If there are no votes, returns None
        """
        if self.votes.count():
            positives = [x for x in self.votes.all() if x.positive]
            return int(100*len(positives)/self.votes.count())

    @property
    def as_endpoint(self):
        """Returns this object as it should be seen via API endpoint"""
        ret = {
            "per": self.per,
        }
        if self.min == self.max:
            ret["amount"] = self.min
        else:
            ret["min"] = self.min
            ret["max"] = self.max
        if self.note:
            ret["note"] = self.note
        if self.source:
            ret["source"] = self.source

        return ret

    @property
    def average(self):
        return (self.min + self.max) / 2


class FootprintVote(_Vote):
    """ User content: Vote fot footprint. User can vote up or down """
    footprint = models.ForeignKey(Footprint, related_name="votes", on_delete=models.CASCADE)
    positive = models.BooleanField(default=False, null=False)  # True: voted up, False: voted down


class Attribute(models.Model):
    """ One entity attribute, e.g. 'weight' for T-shirt """
    name = models.TextField()
    value = models.FloatField()
    unit = models.TextField()
    entity = models.ForeignKey(Entity, related_name="attributes", on_delete=models.CASCADE)

    @property
    def as_endpoint(self):
        """Returns this object as it should be seen via API endpoint"""
        return {
            "name": self.name,
            "value": self.value,
            "unit": self.unit
        }


class ClassifiedQuery(models.Model, _SlugQueryMixin):
    """Here are automatically added all new search phrases, with default behaviour that they are "legal", that means
       that they should be answered. Admin (in "data" application) can later
       classify the phrase as illegal (misprint, spam...)
       Contains only "basic" queries ("Milk"), not composed ones, like comparisons "Milk vs Coffee"
    """
    query = models.TextField()
    t = models.DateTimeField(default=timezone.now)
    is_legal = models.BooleanField(default=True, null=False)

    def __str__(self):
        return self.query


class Query(models.Model):
    """Stores all user search strings.
       This means either decoded parts of comparison queries, e.g. "Chile" or "USA",
       and also raw queries, in case that search string was not recognized as comparison.
       Note: seems to be slightly messy with StatComparison and StatQuery.
       Possibly it should store really "raw" question and add info how it was parsed?
    """
    query = models.TextField()
    ip = models.GenericIPAddressField(null=True)  # ip address of end user
    answered = models.BooleanField(default=False)  # True, if at least one result was returned
    t = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.query


class StatComparison(models.Model, _SlugQueryMixin):
    """Stat for 'comparison' queries: how many times they were searched for"""
    query = models.TextField()
    created = models.DateTimeField(default=timezone.now)
    frequency = models.IntegerField(default=0)  # how many times it was used

    @property
    def slug(self):
        return self.query.replace(" ", "+")


class StatQuery(models.Model, _SlugQueryMixin):
    """Stat for 'normal' queries: how many times they were searched for"""
    query = models.TextField()
    created = models.DateTimeField(default=timezone.now)
    frequency = models.IntegerField(default=0)  # how many times it was used


class Tag(models.Model):
    name = models.CharField(unique=True, max_length=50)

    def __str__(self):
        return self.name
