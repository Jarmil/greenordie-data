# Generated by Django 4.1.7 on 2023-11-06 17:56

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):
    dependencies = [
        ("web", "0013_aliasvote"),
    ]

    operations = [
        migrations.CreateModel(
            name="EntityVote",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.TextField()),
                ("is_entity", models.BooleanField()),
                ("ip", models.GenericIPAddressField(null=True)),
                ("t", models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
    ]
