# Generated by Django 4.1.7 on 2023-11-06 10:31

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):
    dependencies = [
        ("web", "0012_classifiedquery_is_legal"),
    ]

    operations = [
        migrations.CreateModel(
            name="AliasVote",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.TextField()),
                ("ip", models.GenericIPAddressField(null=True)),
                ("t", models.DateTimeField(default=django.utils.timezone.now)),
                (
                    "entity",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="possible_aliases",
                        to="web.entity",
                    ),
                ),
            ],
        ),
    ]
