from django import forms

from . import models


class AddFootprint(forms.ModelForm):

    amount = forms.FloatField(required=True, min_value=0)
    username = forms.CharField(required=True, max_length=100, label="Your name")
    email = forms.EmailField(required=True, label="Your email")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["note"].required = False

    class Meta:
        model = models.Footprint
        fields = ["per", "source", "note"]
