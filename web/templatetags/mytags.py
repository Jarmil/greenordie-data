# Own template tags

from django import template


register = template.Library()


@register.filter
def is_url(value):
    """ Returns True if value seems to be URL """
    if value[:4] == "http":
        return True


@register.filter
def hide_none(value):
    """Hides None value in templates. Usage:
            {{ record.foo | hide_none }}
    """
    return "" if value is None else value


@register.filter
def round_human(value):
    """Makes a sane rounding for carbon footprint results, number of decimal digits depends on amount
            {{ record.footprint.amount | round_human }}
    """
    if value >= 100:
        return str(int(value))
    elif value >= 10:
        return str(round(value, 1))
    elif value >= 1:
        return str(round(value, 2))
    else:
        return str(round(value, 3))

