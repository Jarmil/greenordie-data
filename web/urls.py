from django.urls import path
from django.views.generic import TemplateView, RedirectView

from . import views

urlpatterns = [

    # Static pages
    path("", views.IndexView.as_view(), name="index"),
    path("index", RedirectView.as_view(pattern_name="index")),  # Google thing, for some reason it wants to index it

    path("help", TemplateView.as_view(template_name="help.html"), name="help"),
    path("repos/data", TemplateView.as_view(template_name="repos-data.html"), name="repos-data"),
    path("thankyou", TemplateView.as_view(template_name="thankyou.html"), name="thankyou"),

    # Search things
    path("q/<str:search>", views.query, name="search"),
    path("search", views.search_form, name="search-form"),

    # Other
    path("tag/<str:name>", views.TagView.as_view(), name="tag"),

    # User content
    path("vote/alias", views.vote_for_alias, name="vote-alias"),
    path("vote/entity", views.vote_for_entity, name="vote-entity"),
    path("vote/footprint", views.vote_for_footprint, name="vote-footprint"),
    path("vote/tag/<int:entity_pk>", views.VoteTagView.as_view(), name="vote-tag"),
    path("vote/tag/list", views.VoteTagList.as_view(), name="vote-tag-list"),
    path("vote/tag/do", views.vote_for_tag, name="vote-tag-do"),
    path("footprint/add/<int:entity_pk>", views.AddFootprintFormView.as_view(), name="footprint-add"),

]

