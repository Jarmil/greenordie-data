import yaml
from django.core.management.base import BaseCommand

from web import models


def import_data():
    with open("fixtures/emissions.yaml", "r") as f:
        data = yaml.safe_load(f)

    for x in data:
        entity, created = models.Entity.objects.get_or_create(name=x["name"])
        if created:
            print(f"Entity {x['name']} created")
        if entity.footprints.count() == 0:
            models.Footprint.objects.create(
                entity=entity,
                min=x["emissions"],
                max=x["emissions"],
                per=x["per"],
                source=x["source"],
                note=x.get("note", None),
            )
            print(f"no emissions for {x['name']} found, adding one")


class Command(BaseCommand):
    help = """ Imports data from fixtures/emissions.yaml . 
                Does not rewrite, just add nonexistinge entities and footprints (if entity has no footprint) """

    def handle(self, *args, **options):
        import_data()
