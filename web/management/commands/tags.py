import json
import logging
from django.core.management.base import BaseCommand
import sys
import os

sys.path.append(os.getcwd() + '/../../..')

from web.models import Entity, Tag


class Command(BaseCommand):
    help = """ Tag entities based on old raw json database"""

    def countries(self):
        with open("raw/countries.json", "r") as f:
            data = json.load(f)

        tag_record, _ = Tag.objects.get_or_create(name="country")

        for key, value in data.items():
            try:
                record = Entity.objects.get(name=key)
            except:
                continue

            record.tags.add(tag_record)
            record.save()
            print(key, record)

    def cars(self):
        tag_record, _ = Tag.objects.get_or_create(name="car")

        for record in Entity.objects.all():
            found = False
            for carname in ["audi", "volkswagen", "volvo", "toyota", "suzuki", "subaru", "bmw", "alfa romeo", "aston martin", "abarth", "bentley", "skoda", "saab", "rover", "seat"]:
                if carname in record.name.lower():
                    record.tags.add(tag_record)
                    record.save()
                    found = True
                    # print(record)

            if not found:
                print(f"Not found {record}")

    def handle(self, *args, **options):
        logging.info("Start: tags")
        self.cars()
        self.countries()
        logging.info("End: tags")
