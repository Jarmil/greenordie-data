/* Basically search component */

const as_permalink = str => {
    return str ? ("https://greenordie.org/q/" + encodeURI(str.replaceAll(" ", "_"))) : ""
}

/* If <query> seems to be a comparison (e.g. "car vs train  "), returns list of names to be compared,
   (e.g. ["car", "train"]), both strings trimmed.
*/
const parsed_versus = query => {
    for (delimiter of [" vs ", " versus ", " versus. ", " vs. ", " x "]) {
        const position = query.toLowerCase().indexOf(delimiter)
        if(position >= 0 ) {
             return [
                query.substring(0, position).trim(),
                query.substring(position + delimiter.length).trim(),
             ]
        }
    }
}

// Returns full query URL
const query_url = query => {
    return "/api/v1/q/" + encodeURI(query)
}

/* 'Spinning wheel' displayed when loading something */
const Loading = {
  view: vnode => { return m('.my-5.mx-auto.w-50.text-center.p-3', m('img', { src: '/assets/img/spinner.gif', width: 80 })) }
}


/* Component with one result. Used when there is only one result returned. Attrs: result (object) */
const Result = {
    view: vnode => {
        const x = vnode.attrs.result
        const fp = x.footprint
        const range = fp.amount ? fp.amount.toFixed(3) : (fp.min.toFixed(3) + " - " + fp.max.toFixed(3))
        const fpStr = range + " kg CO2 per 1 " + x.footprint.per
        const fpNote = fp.note ? m(".p-0.pl-3.italic.small", fp.note) : null
        let fpSource = null
        if (fp.source) {
            if (fp.source.startsWith("http")) {
                fpSource = m("a", {href: fp.source, target: "_blank"}, fp.source)
            } else {
                fpSource = fp.source
            }
        }

        // Build small "person" icons, every icon stands for sustainable 3.5 kg/person/day
        const average = fp.amount ? fp.amount : ((fp.max + fp.min) / 2)
        const units = average / 3.5
        let unitIcons = []
        for (var i=0; i<Math.floor(units); i++) {
            unitIcons.push(m("i.fa-solid.fa-sharp.fa-solid.fa-person.big.mr-1", {style: "color: #e01b24;", title: "Every 'person' symbol stands for 3.5 kg of CO2 (sustainable emission limit per person/day)"}))
        }

        const fpSourceFull = fpSource ? m(".d-flex.small.pl-3.italic", m(".mr-2", "Source:"), fpSource) : null
        return m(".border.border-danger.p-2.mt-4",
                    m(".p-0.bold.border-bottom.border-danger.pl-2", x.name),
                    m(".p-0.italic", x.desc),
                    m(".p-3", fpStr),
                    fpNote,
                    fpSourceFull,
                    unitIcons ? m(".ml-3.mt-2", unitIcons) : null,
                )
    }
}


/* Component with small result. Used when there is more results, so we want to display just name and clickable link
   Attrs: result (object)
*/
const SmallResult = {
    view: vnode => {
        const x = vnode.attrs.result
        const fp = x.footprint
        const range = fp.amount ? fp.amount.toFixed(3) : (fp.min.toFixed(3) + " - " + fp.max.toFixed(3))
        const fpStr = range + " kg CO2 per 1 " + x.footprint.per
        const fpNote = fp.note ? m(".p-0.pl-3.italic.small", fp.note) : null
        let fpSource = null
        if (fp.source) {
            if (fp.source.startsWith("http")) {
                fpSource = m("a", {href: fp.source, target: "_blank"}, fp.source)
            } else {
                fpSource = fp.source
            }
        }

        return m(".p-0.mt-2.ml-4",
                    m("a", {href: as_permalink(x.name)}, x.name)
                )
    }
}

const searchForm = {

    oninit: vnode => {

        // If there is q=<search_term> parameter in URL, search for it
        const urlParams = new URLSearchParams(window.location.search)
        const query_from_param = urlParams.get("q")

        // Or if URL is in form /q/search_term, use this
        const splittedPath = window.location.pathname.split("/")
        const path = (splittedPath.length == 3) ? splittedPath[1] : null
        const query_from_url = (path == "q") ? splittedPath[2] : null

        // URL parameter got priority. Use it and replace _ to spaces
        const query = query_from_param ? query_from_param : query_from_url
        const replacedQuery = query ? query.replaceAll("_", " ") : null

        if(query) {
            vnode.state.search = replacedQuery
            searchForm.search(vnode)
        }

        const url = "/api/v1/last"
        m.request(url).then( data => {
            vnode.state.last = data
        })

    },

    set_title: vnode => {
        document.title = (vnode.state.lastSearched ? vnode.state.lastSearched + " @ " : "") + "GreenOrDie.org"
    },

    search: vnode => {
        const searched = vnode.state.search.trim()
        vnode.state.lastSearched = searched

        // Clear results before any query
        vnode.state.results = null
        vnode.state.comp1 = null
        vnode.state.comp2 = null

        vnode.state.loading = true

        const comparison = parsed_versus(searched)
        if(comparison) {

            // Comparsion, e.g. "car vs train" means 2 queries to endpoint. Data are loaded when last of queries finishes.
            m.request(query_url(comparison[0])).then(data=>{
                vnode.state.comp1 = data.results[0]
                if (vnode.state.comp1 && vnode.state.comp2) {
                    vnode.state.loading = false
                    searchForm.set_title(vnode)
                }
            })
            m.request(query_url(comparison[1])).then(data=>{
                vnode.state.comp2 = data.results[0]
                if (vnode.state.comp1 && vnode.state.comp2) {
                    vnode.state.loading = false
                    searchForm.set_title(vnode)
                }
            })
        } else {

            // No comparsion - simple query, e.g. "car"
            m.request(query_url(searched)).then(data=>{
                vnode.state.results = data.results
                vnode.state.loading = false
                searchForm.set_title(vnode)
            })
        }

    },

    view: vnode => {

        const lastResults = vnode.state.last ?
            vnode.state.last.map(x=> m(".my-0.ml-3.small.p-0", m("a", { "href": as_permalink(x) }, x)))
            : null
        const lastResultsArea = lastResults ? m(".ml-0.small", "People searched for:", lastResults) : null

        let resultsArea = null
        if (vnode.state.results) {

            if (vnode.state.results.length == 1) {
                // Exact match found, dissplay it in it's fullness
                let results = vnode.state.results.map(x=> { return m(Result, {result: x}) })
                resultsArea = m(".mt-2", results)
            } else {
                // Found more than one match. Let user select one
                let results = vnode.state.results.map(x=> { return m(SmallResult, {result: x}) })
                resultsArea = m(".mt-2",
                                m(".mt-4.mb-1", "No exact result for '" + vnode.state.lastSearched +"'. Did you mean:"),
                                results)
            }

        }

        const results = vnode.state.results
            ? (vnode.state.results.length == 1 ? vnode.state.results.map(x=> { return m(Result, {result: x}) }) : vnode.state.results.map(x=> { return m(SmallResult, {result: x}) }) )
            : null

        const comparisonUnitInfo = (vnode.state.comp1 && vnode.state.comp2) ?
            ((vnode.state.comp1.footprint.per == vnode.state.comp2.footprint.per) ?
                m(".mt-3.p-2.text-center.italic", "Results are of the same units, thus they are directly comparable.") :
                m(".mt-3.p-2.text-center.border.border-warning.italic", "NOTE: Units are different - consider it before comparing results.") ) : null

        const comparisonArea = (vnode.state.comp1 && vnode.state.comp2) ?
            m(".mt-2",
                m(".text-center.mt-4", "COMPARISON"),
                m( ".row",
                    m(".col-md-6.col-12", m(Result, {result: vnode.state.comp1})),
                    m(".col-md-6.col-12", m(Result, {result: vnode.state.comp2})),
                ),
                comparisonUnitInfo,
            ) :
            null

        const rl = vnode.state.results ? vnode.state.results.length : null
        const noResults = (rl == 0) ?
            m(".border.border-danger.bg-dark.p-3.mt-4",
                m("div", "We have no data for '" + vnode.state.lastSearched + "'. Can you fix it?"),
                m("a", {href: "/repos/data"}, "Check out the data project")) :
            null

        const form = m(".d-flex.flex-wrap.text-white.mt-md-5.mt-3",
            m(".mr-2", "Find carbon footprint:"),
            m("input.mr-1.mb-md-0.mb-2.pb-1", {
                placeholder: "milk, audi a1, china vs usa...",
                onchange: e => {vnode.state.search=e.target.value},
                onkeypress: e => { if(e.key=="Enter"){vnode.state.search=e.target.value; searchForm.search(vnode)} },
            }),
            m("button.fa-solid.fa-magnifying-glass.btn.btn-light.px-5.px-md-4.py-md-0.py-3", {onclick: e => {searchForm.search(vnode)} }),
        )

        const permalink = ( (vnode.state.results && vnode.state.results.length==1) || comparisonArea)
            ?  m(".mt-3",
                    "Permalink: ",
                    m("a", { "href": as_permalink(vnode.state.lastSearched) }, as_permalink(vnode.state.lastSearched)))
            : null

        return m(".p-md-0.py-2.px-md-2.px-0",
            form,
            (results || comparisonArea || vnode.state.loading) ? null : lastResultsArea,
            (vnode.state.loading ? m(Loading) : null),
            resultsArea,
            comparisonArea,
            permalink,
            noResults
        )
    }
}
