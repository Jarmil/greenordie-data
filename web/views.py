import random
import re

from django.db.models import ObjectDoesNotExist
from django.views.generic import TemplateView, ListView
from django.views.generic.edit import FormView
from django.shortcuts import render, redirect
from django.contrib import messages
from django.conf import settings
from django.urls import reverse_lazy
from django.contrib.auth.models import User
import logging
import urllib

from . import models, forms
from web.helpers import client_ip_address


def normalize_query(q):
    """ Unescape URL space coded old way, removes all delimiters used instead of space """
    q = urllib.parse.unquote(q).lower()
    for delimiter in ["_", "+", "%20"]:
        q = q.replace(delimiter, " ")
    return q.strip()


class IndexView(TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["popular_comparisons"] = models.StatComparison.objects.order_by("-frequency")[:10]
        context["popular_queries"] = models.StatQuery.objects.exclude(query__in=models.ClassifiedQuery.objects.filter(is_legal=False).values("query")).order_by("-frequency")[:10]
        context["missing_data"] = models.ClassifiedQuery.objects.filter(is_legal=True).order_by("?")[:10]
        context["missing_tags"] = models.Entity.objects.filter(tags__isnull=True).order_by("?")[:10]
        context["missing_footprint"] = models.Entity.objects.filter(footprints__isnull=True).order_by("?")[:10]
        context["tags"] = models.Tag.objects.order_by("name")
        return context


def get_distance(search):
    """ If query (e.g. " 45 km" or "17 miles"
        was for distance, returns distnace in kms.
        Othervise returns None
    """
    q = search.lower()
    result = re.match(r"([0-9]+)\s+(km|kms|kilometers)", q)
    if result:
        try:
            return int(result.group(1))
        except TypeError:
            return None
    result = re.match(r"([0-9]+)\s+(mile|miles|mi)", q)
    if result:
        try:
            return int( int(result.group(1)) * settings.MILES_TO_KM)
        except TypeError:
            return None


def save_simple_query(search):
    comp, _ = models.StatQuery.objects.get_or_create(query=search)
    comp.frequency += 1
    comp.save()


def query(request, search):
    """results from urls like /q/usa """

    logging.debug(f"Searching for '{search}'")
    search = normalize_query(search)

    context = {"q": search, "permalink": search.replace(" ", "+")}

    # is is a 123 km or 45 miles search?
    distance = get_distance(search)
    if distance:

        save_simple_query(search)

        context["distance_kms"] = distance
        context["distance_miles"] = int(distance / settings.MILES_TO_KM)

        def variant_from_query(q, name):
            result = models.search(q)[0]
            fp = result.footprints.all()[0].average
            return {
                "vehicle": name,
                "per_km": fp,
                "query": q,
                "per_mile": fp / settings.MILES_TO_KM,
                "total": fp * distance,  # note lexical closure
            }

        context["footprints"] = [
            variant_from_query("car", "Personal car (gasoline or diesel)"),
            variant_from_query("suv", "SUV (gasoline or diesel)"),
            variant_from_query("plane", "Plane (economy class)"),
            variant_from_query("motorbike", "Motorbike"),
            variant_from_query("electric car", "Electric car"),
            variant_from_query("bus", "Bus (gasoline or diesel)"),
            variant_from_query("train", "Train"),
            variant_from_query("ferry", "Ferry (foot passenger)"),
        ]

        return render(request, "results/distance.html", context=context)

    # is this a "X versus Y" search?
    for delimiter in [" vs ", " versus ", " versus. ", " vs. ", " x "]:
        if delimiter in search.lower():

            # Save to stats
            comp, _ = models.StatComparison.objects.get_or_create(query=search)
            comp.frequency += 1
            comp.save()

            parts = search.split(delimiter, 2)
            parts = [x.strip() for x in parts]
            # hashtable (instead of list) is kind of HACK - using lists in jinja2 templates is complicated
            try:
                context["comparison"] = {
                    "0": models.search(parts[0])[0],
                    "1": models.search(parts[1])[0],
                }
            except LookupError:
                # one of the parts probably not exists.
                # IMP return sane error message
                pass

            if context.get("comparison", None):
                return render(request, "results/comparison.html", context=context)

    # No, this is a plain search
    save_simple_query(search)
    context["results"] = models.search(search)
    if context.get("results", None):
        return render(request, "results/standard.html", context=context)

    # nothing found. Extend context for possible aliases, so user can vote for one of them
    context["possible_aliases"] = [x for x in models.Entity.objects.all() if x.distance_from(search) <= 3]
    return render(request, "results/nothing.html", context=context)


def search_form(request):
    """results from search form - just "redirect" to query """
    return query(request, request.POST["search"])


class TagView(ListView):
    template_name = "tag.html"
    paginate_by = 15

    def get_queryset(self):
        return models.Tag.objects.get(name=self.kwargs["name"]).entities.all()

    def dispatch(self, request, *args, **kwargs):
        try:
            return super().dispatch(request, *args, **kwargs)
        except ObjectDoesNotExist:
            messages.error(request, "This tag does not exist")
            return redirect("index")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["tag"] = models.Tag.objects.get(name=self.kwargs["name"])
        return context


class VoteTagView(TemplateView):
    template_name = "vote_tag.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["entity"] = models.Entity.objects.get(id=self.kwargs["entity_pk"])
        context["tags"] = models.Tag.objects.all()
        return context


class VoteTagList(TemplateView):
    """ List of entities without tags. After successful vote for tag, user is redirected here so he/she
        can easily add other votes
    """
    template_name = "vote_tag_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["missing_tags"] = models.Entity.objects.filter(tags__isnull=True).order_by("?")[:30]
        return context


def vote_for_alias(request):
    """ Process user response to query:
            is <query> alias for <entity>?
    """
    entity = request.POST.get("entity", None)
    query = request.POST.get("query", None)
    if entity and query:
        models.AliasVote(
            name=query,
            entity=models.Entity.objects.get(pk=entity),
            ip=client_ip_address(request)
        ).save()
    return redirect("thankyou")


def vote_for_entity(request):
    """ Process user response to query:
            Should we list <query>? e.g. is it real entity or comparison or so?
    """
    query = request.POST.get("query", None)
    answer = request.POST.get("answer", None)
    if query and (answer is not None):
        models.EntityVote(
            name=query,
            is_entity=answer,
            ip=client_ip_address(request)
        ).save()
    return redirect("thankyou")


def vote_for_footprint(request):
    """ Process user click on thumbs up or down on particular footprint """
    footprint = models.Footprint.objects.get(pk=request.POST.get("footprint", None))
    models.FootprintVote(
        footprint=footprint,
        positive=int(request.POST.get("positive", None)),
        ip=client_ip_address(request)
    ).save()
    return redirect("thankyou")


def vote_for_tag(request):
    """ Process user response to query: Should <entity_pk> have tag (belong to category) we list <tag_pk>? """
    entity = models.Entity.objects.get(id=request.POST.get("entity_pk", None))
    tag = models.Tag.objects.get(id=request.POST.get("tag_pk", None))
    models.TagVote(entity=entity, tag=tag, ip=client_ip_address(request)).save()
    messages.info(request, f"Thank you! Your proposal for '{entity.name}' -> '{tag.name}' has been added. Can you add more?")
    return redirect("vote-tag-list")


class AddFootprintFormView(FormView):
    model = models.Tag
    template_name = "add-footprint.html"
    success_url = reverse_lazy("index")
    form_class = forms.AddFootprint

    @property
    def entity(self):
        return models.Entity.objects.get(id=self.kwargs["entity_pk"])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["entity"] = self.entity
        return context

    def form_valid(self, form):

        # Handle users. Now they are listed just for evidence. Username is randomly generated
        email = form.cleaned_data["email"]
        lastname = form.cleaned_data["username"]
        users = User.objects.filter(last_name=lastname, email=email)
        if len(users) == 1:
            user = users[0]
        else:
            # Found more or zero users, so we have to create a new one
            username = "User-" + "".join([random.choice("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") for _ in range(10)])
            user = User.objects.create_user(username=username, email=email, last_name=lastname)

        footprint = form.save(commit=False)
        footprint.min = form.cleaned_data["amount"]
        footprint.max = form.cleaned_data["amount"]
        footprint.entity = self.entity
        footprint.user = user
        footprint.save()

        messages.info(self.request, f"Footprint for {footprint.entity.name} was added. Thank you!")
        return super().form_valid(form)
