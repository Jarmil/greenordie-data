from django.urls import path, reverse_lazy
from django.contrib.auth.models import User

from web import models
from . import views

urlpatterns = [

    path("", views.StatView.as_view(), name="stat"),

    path("aliases", views.AliasesView.as_view(), name="aliases"),
    path("aliases/do", views.aliases_do, name="aliases-do"),

    path("tags", views.LoginListView.as_view(model=models.Tag, template_name="tags.html"), name="tag-list"),

    path("tags/add", views.TagAddFormView.as_view(), name="tag-add"),
    path("tags/votes/<int:tag_pk>", views.TagVotesView.as_view(), name="tag-votes"),
    path("tags/delete/<int:pk>", views.LoginDeleteView.as_view(model=models.Tag, success_url=reverse_lazy("tag-list")), name="tag-delete"),
    path("tags/votes/delete/<int:pk>", views.LoginDeleteView.as_view(model=models.TagVote, success_url=reverse_lazy("tag-list")), name="tag-vote-delete"),
    path("tags/votes/assign", views.tag_vote_assign, name="tag-vote-assign"),

    path("nodata", views.NoDataView.as_view(), name="nodata"),

    path("sort", views.SortView.as_view(), name="sort"),
    path("sort/alias/<int:pk>", views.SortFindAliasView.as_view(), name="sort-alias"),
    path("sort/alias/do", views.sort_alias_do, name="sort-alias-do"),
    path("sort/delete/<int:pk>", views.sort_delete, name="sort-delete"),
    path("sort/spam/<int:pk>", views.sort_spam, name="sort-spam"),
    path("sort/add/<int:pk>", views.sort_add, name="sort-add"),

    path("footprints", views.FootprintListView.as_view(), name="footprint-list"),
    path("footprint-votes", views.FootprintVotesListView.as_view(), name="footprint-votes-list"),
    path("footprints/delete/<int:pk>", views.LoginDeleteView.as_view(model=models.Footprint, success_url=reverse_lazy("footprint-list")), name="footprint-delete"),
    path("footprints/approve", views.footprint_approve, name="footprint-approve"),

    path("users", views.LoginListView.as_view(model=User, template_name="users.html"), name="user-list"),

    path("comparison/delete/<int:pk>", views.LoginDeleteView.as_view(model=models.StatComparison, success_url=reverse_lazy("index")), name="statcomparison-delete"),
    path("query/delete/<int:pk>", views.LoginDeleteView.as_view(model=models.StatQuery, success_url=reverse_lazy("index")), name="statquery-delete"),
]
