from django import forms

from web import models


class TagEdit(forms.ModelForm):

    class Meta:
        model = models.Tag
        fields = ["name"]
