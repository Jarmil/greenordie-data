from django.views.generic import TemplateView, ListView, DeleteView
from django.views.generic.edit import FormView
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.urls import reverse_lazy

from web import models
from . import forms


class LoginDeleteView(LoginRequiredMixin, DeleteView):
    pass


class LoginListView(LoginRequiredMixin, ListView):
    pass


class StatView(LoginRequiredMixin, TemplateView):
    """ Usage statistics """
    template_name = "queries.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["last"] = models.Query.objects.all().order_by("-t")[:10]
        return context


class AliasesView(LoginRequiredMixin, TemplateView):
    """ List alias votes  for decision, if they should be added as alias or not """
    template_name = "aliases.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["objects"] = models.AliasVote.objects.exclude(name__in=models.Alias.objects.values("name")).order_by("-t")
        return context


@login_required
def aliases_do(request):
    """ Process admins response to query: Should <name> be an <alias> for an <entity>? """
    name = request.POST.get("name", None)
    entity_pk = request.POST.get("entity", None)
    answer = request.POST.get("answer", None)
    if name and entity_pk and (answer is not None):
        if int(answer) == 1:
            entity = models.Entity.objects.get(pk=entity_pk)
            models.Alias(name=name, entity=entity, comment=f"Added by admin {request.user}").save()
            messages.info(request, f"Added alias '{name}' -> '{entity}'")
        else:
            models.AliasVote.objects.filter(name=name).delete()
            messages.info(request, f"Deleted alias votes for '{name}'")
    return redirect("aliases")


class TagAddFormView(LoginRequiredMixin, FormView):
    model = models.Tag
    template_name = "tag-edit.html"
    success_url = reverse_lazy("tag-list")
    form_class = forms.TagEdit

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


class TagVotesView(LoginRequiredMixin, TemplateView):
    """ List entity-tag votes for decision, if they should be added as alias or not """
    template_name = "tag-votes.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tag = models.Tag.objects.get(id=self.kwargs["tag_pk"])
        context["tag"] = tag
        context["votes"] = models.TagVote.objects.filter(tag=tag).order_by("entity")
        return context


@login_required
def tag_vote_assign(request):
    """ Process admins request to assign Tag to entity based on EntityVote with <pk> """
    vote = models.TagVote.objects.get(id=request.POST["pk"])
    vote.entity.tags.add(vote.tag)
    vote.entity.save()
    vote.delete()
    messages.info(request, f"Added Entity '{vote.entity.name}' to category (tag) '{vote.tag.name}', corresponding vote deleted")
    return redirect("tag-votes", tag_pk=int(request.POST["tag"]))


class NoDataView(LoginRequiredMixin, TemplateView):
    """ List entities having no footprint data """
    template_name = "nodata.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["objects"] = models.Entity.objects.filter(footprints__isnull=True)[:50]
        return context


class SortView(LoginRequiredMixin, TemplateView):
    """ List queries, that were classified as "Should be answered" for decision, if they are alias or new entity """
    template_name = "sort.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["objects"] = models.ClassifiedQuery.objects.filter(is_legal=True).order_by("-t")
        return context


class SortFindAliasView(LoginRequiredMixin, TemplateView):
    """ Shows alias search form for ClassifiedQuery <pk> """
    template_name = "find-alias.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["object"] = models.ClassifiedQuery.objects.get(pk=self.kwargs["pk"])
        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context["objects"] = models.Entity.objects.filter(name__icontains=self.request.POST["search"])
        return self.render_to_response(context)


@login_required
def sort_alias_do(request):
    """ Process admins request to add an alias <name> to <entity_pk> """
    name = request.POST.get("name", None)
    entity_pk = request.POST.get("entity_pk", None)
    classified_query_pk = request.POST.get("classified_query_pk", None)
    if all([name, entity_pk, classified_query_pk]):
        entity = models.Entity.objects.get(pk=entity_pk)
        models.Alias(name=name, entity=entity, comment=f"Added by admin {request.user}").save()
        models.ClassifiedQuery.objects.get(pk=classified_query_pk).delete()
        messages.info(request, f"Added Alias '{name}' -> '{entity}'")
        messages.warning(request, f"Deleted ClassifiedQuery {classified_query_pk}")
    return redirect("sort")


@login_required
def sort_delete(request, pk):
    """ Delete ClassifiedQuery """
    models.ClassifiedQuery.objects.get(pk=pk).delete()
    messages.warning(request, f"ClassifiedQuery {pk} deleted")
    return redirect("sort")


@login_required
def sort_spam(request, pk):
    """ Mark ClassifiedQuery as "not to be answered" """
    q = models.ClassifiedQuery.objects.get(pk=pk)
    q.is_legal = False
    q.save()
    messages.warning(request, f"ClassifiedQuery '{q.query}' marked as spam")
    return redirect("sort")


@login_required
def sort_add(request, pk):
    """ Add a new entity from ClassifiedQuery (and delete corresponding ClassifiedQuery) """
    q = models.ClassifiedQuery.objects.get(pk=pk)
    models.Entity(name=q.query).save()
    q.delete()
    messages.info(request, f"New Entity '{q.query}' created")
    messages.warning(request, f"ClassifiedQuery {pk} deleted")
    return redirect("sort")


class FootprintListView(LoginRequiredMixin, ListView):

    template_name = "footprints.html"

    def get_queryset(self):
        return models.Footprint.objects.filter(reviewed=False)


class FootprintVotesListView(LoginRequiredMixin, ListView):

    template_name = "footprint-votes.html"

    def get_queryset(self):
        return models.FootprintVote.objects.all().order_by("-t")[:50]


@login_required
def footprint_approve(request):
    x = models.Footprint.objects.get(id=request.POST["pk"])
    x.reviewed = True
    x.save()
    messages.info(request, f"Footprint for '{x.entity.name}' was approved")
    return redirect("footprint-list")
