"""
Simple SPARQL import tool

Reads SPARQL query from wikidata.org, formats data to YAML structure and prints it to stdout
Use it as a raw import, values has to be validated manually for "per" units (and sometimes for
main unit, footprint is listed in kilograms, grams etc)

Usage:
  wikimport.py [options]

Options:
    -h      help
    -l      list of unique Wikidata entities having CO2 emmissions defined
"""

from SPARQLWrapper import SPARQLWrapper, JSON
import docopt

LIMIT = 9999


def sparql_query(query):

    sparql = SPARQLWrapper("https://query.wikidata.org/sparql")
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    result = sparql.query().convert()

    for hit in result["results"]["bindings"]:
        yield hit


if __name__ == "__main__":

    args = docopt.docopt(__doc__)

    # Query everyhing having some carbon dioxide footprint
    query = """SELECT DISTINCT ?item ?itemLabel ?footprint WHERE {
      ?item p:P5991 ?statement0;
      wdt:P5991 ?footprint. 
      ?statement0 (psv:P5991/wikibase:quantityAmount) ?numericQuantity.
        SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }} LIMIT """ + str(LIMIT)

    if args["-l"]:
        entities = [hit['item']['value'] for hit in sparql_query(query)]
        entities = set(entities)
        print("\n".join(entities))

    else:
        entities = [x for x in sparql_query(query)]
        for hit in entities:
            print(hit['itemLabel']['value'] + ":")
            print("  footprint:")
            print("    value: " + hit['footprint']['value'])
            print("    per: TO DO")
            print("    source: " + hit['item']['value'])
            print()

        print(f"Records found: {len(entities)}")
